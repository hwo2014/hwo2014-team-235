package noobbot;

import com.google.gson.Gson;
import noobbot.message.*;

import java.io.*;
import java.net.Socket;

public class Main {

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        String trackName = null;
        String carCount = null;
        String password = null;

        if (args.length > 4) {
            trackName = args[4];
        }

        if (args.length > 5) {
            carCount = args[5];
        }

        if (args.length > 6) {
            password = args[6];
        }

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        System.out.println("Optional params: " + trackName + ", (" + carCount + ") [" + password + "]");

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey, trackName, carCount, password);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    CarBot car;
    Logger logger = new Logger();

    public Main(final BufferedReader reader, final PrintWriter writer, String botName, String botKey, String track, String carCount, String password) throws IOException {
        this.writer = writer;
        String line = null;

        if (isEmpty(track) && isEmpty(carCount) && isEmpty(password)) {
            send(new JoinMessage(botName, botKey));
        } else if (isEmpty(track) && !isEmpty(password)) {
            send(new JoinRaceMessage(botName, botKey, track, carCount, password));
        } else {
            send(new CreateRaceMessage(botName, botKey, track, carCount, password));
        }

        while ((line = reader.readLine()) != null) {

            MsgWrapper wrapper = gson.fromJson(line, MsgWrapper.class);

            AbstractMessage message = getMessageFor(line, wrapper.msgType);

            if (car != null) {
                car.onMessage(message).stream().forEach(m -> send(m));
            }
        }
    }

    private void send(final AbstractMessage msg) {

        if (msg != null) {
            writer.println(msg.toJson());
            writer.flush();
        }
    }

    private AbstractMessage getMessageFor(String line, String msgType) {

        MessageType messageType = MessageType.valueOfAlias(msgType);

        Class messageClass = getMessageClass(messageType);

        if (messageClass.equals(GameInitMessage.class)) {
            System.out.println("GameInit JSON: " + line);
        }

        AbstractMessage message = (AbstractMessage) new Gson().fromJson(line, messageClass);

        if (messageType == MessageType.yourCar) {
            YourCarMessage yourCarMessage = (YourCarMessage) message;
            car = new CarBot(yourCarMessage.getColor(), yourCarMessage.getName(), logger);
        }

        return message;
    }

    private Class getMessageClass(MessageType messageType) {

        switch (messageType) {

            case join:
                return JoinMessage.class;
            case gameInit:
                return GameInitMessage.class;
            case carPositions:
                return CarPositionsMessage.class;
            case gameStart:
                return GameStartMessage.class;
            case lapFinished:
                return LapFinishedMessage.class;
            case ping:
                return PingMessage.class;
            case yourCar:
                return YourCarMessage.class;
            case gameEnd:
                return GameEndMessage.class;
            case crash:
                return CrashMessage.class;
            case spawn:
                return SpawnMessage.class;
            case dnf:
                return DnfMessage.class;
            case finish:
                return FinishMessage.class;
            case turboAvailable:
                return TurboAvailableMessage.class;
            default:
                return UnknownMessage.class;
        }
    }

    boolean isEmpty(String s) {
        return s == null || "".equals(s);
    }
}


package noobbot;

import com.google.common.collect.Lists;
import noobbot.domain.*;
import noobbot.domain.internal.TrackElement;
import noobbot.message.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vtajzich
 */
public class BestLaneCarBot extends SensoredCar {

    Logger logger;
    Race race;
    int currentLap;
    double trackLength;
    List<TrackElement> trackElements = new ArrayList<>();

    public BestLaneCarBot(String color, String name, Logger logger) {
        super(new CarId(name, color));
        this.logger = logger;
    }

    public double getTrackLength() {
        return trackLength;
    }

    public List<TrackElement> getTrackElements() {
        return trackElements;
    }

    public AbstractMessage onMessage(AbstractMessage message) {

        logger.fromServer(message);

        AbstractMessage response = null;

        if (message == null || message.getMsgType() == null) {
            return new PingMessage();
        }

        switch (message.getMsgType()) {

            case carPositions:
                response = updateOnCarPosition((CarPositionsMessage) message);
                break;
            case gameStart:
                response = new ThrottleMessage(1);
                break;
            case lapFinished:
                break;
            case gameEnd:
                break;
            case gameInit:
                processGameInit((GameInitMessage) message);
            case UNKNOWN:
                response = new PingMessage();
        }

        logger.toServer(response);

        return response;
    }

    void processGameInit(GameInitMessage gameInitMessage) {

        race = gameInitMessage.getData().getRace();

        determineTrackElements();

        trackLength = trackElements.stream().mapToDouble(TrackElement::getLength).sum();
    }

    AbstractMessage updateOnCarPosition(CarPositionsMessage message) {

        CarPosition myPosition = message.getData().stream().filter((CarPosition position) -> position.getId().equals(getId())).findFirst().get();

        PiecePosition piecePosition = myPosition.getPiecePosition();

        ArrayList<Piece> pieces = race.getTrack().getPieces();

        Piece piece = pieces.get(piecePosition.getPieceIndex());

        Piece nextPiece = null;

        int nextIndex = piecePosition.getPieceIndex() + 2;

        if (nextIndex > pieces.size() - 2) {
            nextPiece = pieces.get(1);
        } else {
            nextPiece = pieces.get(nextIndex);
        }

        double absAngle = Math.abs(piece.getAngle());
        double absAngleNext = Math.abs(nextPiece.getAngle());

        double throttle = 0;

        if (absAngleNext >= 44 && absAngle <= 44) {
            throttle = 0.3;
        } else if (absAngle <= 10) {
            throttle = 1;
        } else if (absAngle <= 23) {
            throttle = 1;
        } else if (absAngle >= 44) {
            throttle = 0.65;
        }

        return new ThrottleMessage(throttle);
    }

    private void determineTrackElements() {

//        trackElements = race.getTrack().getPieces().stream()
//                .map((Piece piece) -> new TrackElement(piece.getLength(), piece.getAngle(), piece.getRadius()))
//                .map((TrackElement element) -> Lists.newArrayList(element))
//                .reduce((ArrayList<TrackElement> previous, ArrayList<TrackElement> current) -> {
//
//                    TrackElement previousTrackElement = previous.get(previous.size() - 1);
//                    TrackElement currentTrackElement = current.get(0);
//
//                    if (previousTrackElement.getOriginalAngle() == currentTrackElement.getOriginalAngle()) {
//                        previousTrackElement.plus(currentTrackElement);
//                    } else {
//                        previous.add(currentTrackElement);
//                    }
//
//                    return previous;
//
//                }).get();
    }
}

package noobbot;

import noobbot.domain.internal.TrackElement;

/**
 * Created by vtajzich
 */
public class TrackElementClassifier {

    public static final int MAX_SPEED = 8;
    public static final int CURVE_HIGH_SPEED = 5;
    public static final int CURVE_NORMAL_SPEED = 4;
    public static final double CURVE_SLOW_SPEED = 2d;

    public static double classify(TrackElement current, TrackElement next) {

        if (!current.isCurve() && !next.isCurve()) {
            return MAX_SPEED;
        }

        if (current.isCurve() && current.curveFactor() <= 0.7) {
            return CURVE_NORMAL_SPEED;
        }

        if (next.curveFactor() < 0.7) {
            return CURVE_HIGH_SPEED;
        } else if (next.curveFactor() < 1.5) {
            return CURVE_NORMAL_SPEED;
        } else if (next.curveFactor() >= 1.5) {
            return CURVE_SLOW_SPEED;
        }

        return MAX_SPEED;
    }
}

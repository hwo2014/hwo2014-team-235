package noobbot;

import noobbot.domain.Piece;
import noobbot.domain.internal.TrackElement;

/**
 * Created by vtajzich
 */
public class ThrottleTeKey {

    TrackElement trackElement;
    Piece piece;

    public ThrottleTeKey(TrackElement trackElement, Piece piece) {
        this.trackElement = trackElement;
        this.piece = piece;
    }

    public TrackElement getTrackElement() {
        return trackElement;
    }

    public Piece getPiece() {
        return piece;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThrottleTeKey that = (ThrottleTeKey) o;

        if (!piece.equals(that.piece)) return false;
        if (!trackElement.equals(that.trackElement)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = trackElement.hashCode();
        result = 31 * result + piece.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ThrottleTeKey{" +
                "trackElement=" + trackElement +
                ", piece=" + piece +
                '}';
    }
}

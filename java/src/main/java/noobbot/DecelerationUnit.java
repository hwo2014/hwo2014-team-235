package noobbot;

import com.google.common.util.concurrent.AtomicDouble;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Created by vtajzich
 */
public class DecelerationUnit {

    private double actualSpeed;
    double wantedSpeed;
    double deceleration;
    double lengthLeft;
    private double distancePerTime;

    public DecelerationUnit(double actualSpeed, double wantedSpeed, double deceleration, double lengthLeft, double distancePerTime) {
        this.actualSpeed = actualSpeed;
        this.wantedSpeed = wantedSpeed;
        this.deceleration = deceleration;
        this.lengthLeft = lengthLeft;
        this.distancePerTime = Math.abs(distancePerTime);
    }

    int timeLeft() {

        if (deceleration == 0d || actualSpeed == 0d) {
            return Integer.MAX_VALUE;
        }

        if (lengthLeft == 0) {
            return 0;
        }

        double startSpeed = actualSpeed - wantedSpeed;

        if (startSpeed <= 0) {
            return Integer.MAX_VALUE;
        }

        return sciencistComputation(startSpeed);
    }

    private int sciencistComputation(double startSpeed) {

        AtomicInteger time = new AtomicInteger();
        AtomicDouble speed = new AtomicDouble(Math.abs(startSpeed));

        DecelerationTick tick = null;

        Iterator<DecelerationTick> decelerationTickIterator = Stream.generate(() -> {

            int timeValue = time.incrementAndGet();

            double currentSpeed = speed.get() + (-1 * deceleration * timeValue);
            speed.set(currentSpeed);

            //s=v0 t + 1/2 a t^2
            double track = (currentSpeed * timeValue) + (0.5 * deceleration * Math.sqrt(timeValue));

            return new DecelerationTick(timeValue, currentSpeed, track);
        }).iterator();

        //TODO: is there a way how to terminate stream instead of loop?
        while (decelerationTickIterator.hasNext()) {

            DecelerationTick current = decelerationTickIterator.next();

            if (current.speed <= 0) {
                tick = current;
                break;
            }

            if (current.track >= lengthLeft) {
                break;
            } else {
                tick = current;
            }
        }

        if (tick != null && tick.time * distancePerTime < lengthLeft) {
            return tick.time;
        }

        return 0;
    }

    private int stupidComputation(double startSpeed) {

        double numOfTicksNeeded = startSpeed / deceleration / distancePerTime;

        double numOfTicksAvailable = lengthLeft / distancePerTime;

        if (numOfTicksNeeded < numOfTicksAvailable && numOfTicksNeeded > 0) {
            return ((Long) Math.round(numOfTicksNeeded)).intValue();
        }

        return 0;
    }

    static class DecelerationTick {

        private final double speed;
        int time;
        double track;

        DecelerationTick(int time, double speed, double track) {
            this.time = time;
            this.speed = speed;
            this.track = track;
        }

        @Override
        public String toString() {
            return "DecelerationTick{" +
                    "speed=" + speed +
                    ", time=" + time +
                    ", track=" + track +
                    '}';
        }
    }
}

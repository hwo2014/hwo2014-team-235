package noobbot.domain.internal;

import noobbot.domain.Piece;

/**
 * User: mbednar
 * Date: 17.04.14
 * Time: 19:48
 */
public class LapPiece {
    private int lap;
    private Piece piece;

    public LapPiece(Piece piece) {
        this.piece = piece;
    }

    @Override
    public String toString() {
        return piece.toString();
    }

    public double getLength() {
        return piece.getLength();
    }

    public void setLength(double length) {
        piece.setLength(length);
    }

    public boolean isSwitchPiece() {
        return piece.isSwitchPiece();
    }

    public void setSwitchPiece(boolean switchPiece) {
        piece.setSwitchPiece(switchPiece);
    }

    public int getRadius() {
        return piece.getRadius();
    }

    public void setRadius(int radius) {
        piece.setRadius(radius);
    }

    public double getAngle() {
        return piece.getAngle();
    }

    public void setAngle(double angle) {
        piece.setAngle(angle);
    }

    public int getIndex() {
        return piece.getIndex();
    }

    public void setIndex(int index) {
        piece.setIndex(index);
    }

    public double getAngleAbs() {
        return piece.getAngleAbs();
    }

    public double getLengthForLane(int distanceFromCenter) {
        return piece.getLengthForLane(distanceFromCenter);
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }
}

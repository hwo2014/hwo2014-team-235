package noobbot.domain.internal;

import noobbot.Curve;
import noobbot.domain.Piece;
import noobbot.domain.PiecePosition;
import noobbot.fuzzy.FuzPosition;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by vtajzich
 */
public class TrackElement {

    String id = UUID.randomUUID().toString();

    double length;
    double angle;
    double radius;
    double originalAngle;
    Curve curve;

    boolean cilovaRovinka;

    List<Piece> pieces = new ArrayList<>();

    public TrackElement(Piece piece) {
        this.length = piece.getLength();
        this.angle = piece.getAngle();
        this.radius = piece.getRadius();
        this.originalAngle = angle;

        pieces.add(piece);
    }

    public Piece getPieceFor(PiecePosition position) {
        return pieces.stream().filter(p -> p.getIndex() == position.getPieceIndex()).findFirst().orElse(null);
    }

    public Piece getFirstPiece() {
        return pieces.get(0);
    }

    public Piece getLastPiece() {
        return pieces.get(pieces.size() - 1);
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public double getLength() {
        return getLength(getLastPiece().getIndex(), -1);
    }

    double getLength(PiecePosition position) {
        return getLength(position.getPieceIndex(), position.getInPieceDistance());
    }

    double getLength(int index, double inPieceDistance) {
        return getLength(index, inPieceDistance, false);
    }

    public double getLength(int index, double inPieceDistance, boolean whatIsRest) {

        double result = pieces.stream()
                .filter(p -> {

                    if (whatIsRest) {
                        return p.getIndex() >= index;
                    } else {
                        return p.getIndex() <= index;
                    }
                }).mapToDouble(p -> {

                    if (p.getIndex() == index && inPieceDistance != -1) {

                        if (p.getAngle() == 0) {
                            return p.getLength() - inPieceDistance;
                        } else {

                            double fullLength = p.getAngle() * (Math.PI / 180) * p.getRadius();
                            double percent = fullLength / 100;

                            return fullLength - (percent * inPieceDistance);
                        }

                    } else {
                        if (p.getAngle() == 0) {
                            return p.getLength();
                        } else {
                            return p.getAngle() * (Math.PI / 180) * p.getRadius();
                        }
                    }
                })
                .sum();

        result = Math.abs(result);

        return Math.abs(result);
    }

    public boolean isCurve() {
        return getAngleAbs() > 0;
    }

    public double getAngle() {
        return angle;
    }

    public double getRadius() {
        return radius;
    }

    public double getOriginalAngle() {
        return originalAngle;
    }

    public double getOriginalAngleAbs() {
        return Math.abs(getOriginalAngle());
    }

    public double curveFactor() {
        return getAngleAbs() / getRadius();
    }

    public void plus(TrackElement trackElement) {

        this.length += trackElement.getLength();
        this.angle += trackElement.getAngle();

        pieces.addAll(trackElement.getPieces());
    }

    public boolean containsPiece(Piece piece) {
        return pieces.contains(piece);
    }

    public FuzPosition getPosition(PiecePosition piecePosition) {

        double trackLength = getLength();
        double trackLengthUntilPosition = getLength(piecePosition);

        if (trackLengthUntilPosition == 0) {
            return FuzPosition.IN_THE_BEGINNING;
        } else if (trackLength == trackLengthUntilPosition) {
            return FuzPosition.IN_THE_END;
        } else if (isAroundFromLeft(trackLength / 10, trackLengthUntilPosition)) {
            return FuzPosition.IN_THE_BEGINNING;
        } else if (isAroundFromLeft(trackLength / 6, trackLengthUntilPosition)) {
            return FuzPosition.NEAR_BEGINNIG;
        } else if (isAroundFromLeft(trackLength / 4, trackLengthUntilPosition)) {
            return FuzPosition.FIRST_QUATER;
        } else if (isAroundFromLeft(trackLength / 3, trackLengthUntilPosition)) {
            return FuzPosition.THIRD;
        } else if (isAround(trackLength / 2, trackLengthUntilPosition)) {
            return FuzPosition.HALF;
        } else if (isAround((trackLength / 3) * 2, trackLengthUntilPosition)) {
            return FuzPosition.TWO_THIRD;
        } else if (isAround(trackLength, trackLengthUntilPosition)) {
            return FuzPosition.ALMOST;
        }

        return FuzPosition.IN_THE_END;
    }

    boolean isAround(double trackLength, double around) {

        double percent15 = trackLength / 100 * 15;

        return around > trackLength - percent15 && around < trackLength + percent15;
    }

    boolean isAroundFromLeft(double trackLength, double around) {

        double percent15 = trackLength / 100 * 15;

        return around < trackLength + percent15;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "TrackElement{" +
                "id='" + id + '\'' +
                ", length=" + length +
                ", angle=" + angle +
                ", radius=" + radius +
                ", pieces=" + pieces +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrackElement element = (TrackElement) o;

        if (!id.equals(element.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public double getAngleAbs() {
        return Math.abs(getAngle());
    }

    public Curve getCurve() {

        double angle = getAngleAbs();
        double radius = getRadius();

        if (angle == 0d) {
            return Curve.Zadna;
        } else {

            if (angle <= 30d) {
                return Curve.Mirna;
            } else if (angle <= 90 && radius >= 150) {
                return Curve.Ostra;
            } else if (angle >= 180d && radius <= 150d) {
                return Curve.Ostra;
            } else if (angle >= 180d && radius < 100d) {
                return Curve.Hodne_Ostra;
            } else if (angle >= 180d && radius < 80d) {
                return Curve.Kurva_Ostra;
            }
        }

        return Curve.Mirna;
    }

    public boolean isCilovaRovinka() {
        return cilovaRovinka;
    }

    public void setCilovaRovinka(boolean cilovaRovinka) {
        this.cilovaRovinka = cilovaRovinka;
    }
}

package noobbot.domain;

import noobbot.Car;

import java.util.ArrayList;

/**
 * Created by vtajzich
 */
public class Race {

    Track track;
    ArrayList<Car> cars = new ArrayList<>();
    RaceSession raceSession;

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

    public void setRaceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
    }

    @Override
    public String toString() {
        return "Race{" +
                "track=" + track +
                ", cars=" + cars +
                ", raceSession=" + raceSession +
                '}';
    }
}

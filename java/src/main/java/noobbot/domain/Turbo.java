package noobbot.domain;

/**
 * Created by vtajzich
 */
public class Turbo {

    double turboDurationMilliseconds;
    int turboDurationTicks;
    double turboFactor;

    public double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }

    public void setTurboDurationMilliseconds(double turboDurationMilliseconds) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }

    public int getTurboDurationTicks() {
        return turboDurationTicks;
    }

    public void setTurboDurationTicks(int turboDurationTicks) {
        this.turboDurationTicks = turboDurationTicks;
    }

    public double getTurboFactor() {
        return turboFactor;
    }

    public void setTurboFactor(double turboFactor) {
        this.turboFactor = turboFactor;
    }
}

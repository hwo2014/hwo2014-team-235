package noobbot.domain;

/**
 * Created by vtajzich
 */
public class RaceSession {

    int laps;
    int maxLapTimeMs;
    boolean quickRace;

    public int getLaps() {
        return laps;
    }

    public void setLaps(int laps) {
        this.laps = laps;
    }

    public int getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public void setMaxLapTimeMs(int maxLapTimeMs) {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }

    public void setQuickRace(boolean quickRace) {
        this.quickRace = quickRace;
    }

    @Override
    public String toString() {
        return "RaceSession{" +
                "laps=" + laps +
                ", maxLapTimeMs=" + maxLapTimeMs +
                ", quickRace=" + quickRace +
                '}';
    }
}

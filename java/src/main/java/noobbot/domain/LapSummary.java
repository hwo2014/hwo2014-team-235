package noobbot.domain;

/**
 * Created by vtajzich
 */
public class LapSummary {

    CarId car;
    LapTime lapTime;
    RaceTime raceTime;
    Ranking ranking;

    public CarId getCar() {
        return car;
    }

    public void setCar(CarId car) {
        this.car = car;
    }

    public LapTime getLapTime() {
        return lapTime;
    }

    public void setLapTime(LapTime lapTime) {
        this.lapTime = lapTime;
    }

    public RaceTime getRaceTime() {
        return raceTime;
    }

    public void setRaceTime(RaceTime raceTime) {
        this.raceTime = raceTime;
    }

    public Ranking getRanking() {
        return ranking;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }
}

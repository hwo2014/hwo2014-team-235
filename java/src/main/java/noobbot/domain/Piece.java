package noobbot.domain;

import com.google.gson.annotations.SerializedName;

import static java.lang.Math.PI;
import static java.lang.Math.abs;

/**
 * Created by vtajzich
 */
public class Piece {

    int index;

    double length;

    @SerializedName("switch")
    boolean switchPiece;

    int radius;

    double angle;

    public Piece() {
    }

    public Piece(double length, int radius, double angle, boolean switchPiece) {
        this.length = length;
        this.switchPiece = switchPiece;
        this.radius = radius;
        this.angle = angle;
    }

    public Piece(int index) {
        this.index = index;
    }

    public Piece(int index, double angle, int radius) {
        this.index = index;
        this.angle = angle;
        this.radius = radius;
    }

    public Piece(int index, double length) {
        this.length = length;
        this.index = index;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public boolean isSwitchPiece() {
        return switchPiece;
    }

    public void setSwitchPiece(boolean switchPiece) {
        this.switchPiece = switchPiece;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Piece{" +
                "index=" + index +
                ", length=" + length +
                ", switchPiece=" + switchPiece +
                ", radius=" + radius +
                ", angle=" + angle +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Piece piece = (Piece) o;

        if (index != piece.index) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return index;
    }

    public double getAngleAbs() {
        return Math.abs(getAngle());
    }

    public double getLengthForLane(int distanceFromCenter) {
        if (getRadius() > 0) {
            int laneRadius = getRadius() + (getAngle() > 0 ? -1 : 1) * distanceFromCenter;
            double arcLength = (PI * laneRadius * abs(getAngle())) / 180;
            return arcLength;
        }
        return getLength();
    }

}

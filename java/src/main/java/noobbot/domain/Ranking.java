package noobbot.domain;

/**
 * Created by vtajzich
 */
public class Ranking {

    int overall;
    int fastestLap;

    public int getOverall() {
        return overall;
    }

    public void setOverall(int overall) {
        this.overall = overall;
    }

    public int getFastestLap() {
        return fastestLap;
    }

    public void setFastestLap(int fastestLap) {
        this.fastestLap = fastestLap;
    }
}

package noobbot.domain;

/**
 * Created by vtajzich
 */
public class PiecePosition {

    int pieceIndex;
    double inPieceDistance;
    int lap;
    Lane lane = new Lane();

    public class Lane {
        int startLaneIndex;
        int endLaneIndex;

        public int getStartLaneIndex() {
            return startLaneIndex;
        }

        public void setStartLaneIndex(int startLaneIndex) {
            this.startLaneIndex = startLaneIndex;
        }

        public int getEndLaneIndex() {
            return endLaneIndex;
        }

        public void setEndLaneIndex(int endLaneIndex) {
            this.endLaneIndex = endLaneIndex;
        }

        @Override
        public String toString() {
            return "Lane{" +
                    "startLaneIndex=" + startLaneIndex +
                    ", endLaneIndex=" + endLaneIndex +
                    '}';
        }
    }

    public PiecePosition() {
    }

    public PiecePosition(int pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public PiecePosition(int pieceIndex, double inPieceDistance) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(int pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }

    public Lane getLane() {
        return lane;
    }

    public void setLane(Lane lane) {
        this.lane = lane;
    }

    @Override
    public String toString() {
        return "{" +
                "pieceIndex=" + pieceIndex +
                ", inPieceDistance=" + inPieceDistance +
                ", lap=" + lap +
                ", lane=" + lane +
                '}';
    }
}

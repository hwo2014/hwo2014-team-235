package noobbot.domain;

/**
 * Created by vtajzich
 */
public class Lane {

    int distanceFromCenter;
    int index;

    public int getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(int distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Lane{" +
                "distanceFromCenter=" + distanceFromCenter +
                ", index=" + index +
                '}';
    }
}

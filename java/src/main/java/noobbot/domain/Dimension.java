package noobbot.domain;

/**
 * Created by vtajzich
 */
public class Dimension {

    double length;
    double width;
    int guideFlagPosition;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public int getGuideFlagPosition() {
        return guideFlagPosition;
    }

    public void setGuideFlagPosition(int guideFlagPosition) {
        this.guideFlagPosition = guideFlagPosition;
    }
}

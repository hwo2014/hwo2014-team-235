package noobbot.domain;

/**
 * Created by vtajzich
 */
public class CarPosition {

    CarId id;
    double angle;
    PiecePosition piecePosition;

    public CarPosition() {
    }

    public CarPosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

    public CarId getId() {
        return id;
    }

    public void setId(CarId id) {
        this.id = id;
    }

    public double getAngle() {

        if (Math.abs(angle) < 0.1) {
            angle = 0;
        }

        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

    @Override
    public String toString() {
        return "CarPosition{" +
                "id=" + id +
                ", angle=" + angle +
                ", piecePosition=" + piecePosition +
                '}';
    }

    public double getAngleAbs() {
        return Math.abs(getAngle());
    }
}

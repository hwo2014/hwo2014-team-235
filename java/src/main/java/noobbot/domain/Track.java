package noobbot.domain;

import java.util.ArrayList;

/**
 * Created by vtajzich
 */
public class Track {

    String id;
    String name;
    ArrayList<Piece> pieces = new ArrayList<>();
    ArrayList<Lane> lanes = new ArrayList<>();
    Point startingPoint;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(ArrayList<Piece> pieces) {
        this.pieces = pieces;
    }

    public ArrayList<Lane> getLanes() {
        return lanes;
    }

    public void setLanes(ArrayList<Lane> lanes) {
        this.lanes = lanes;
    }

    public Point getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(Point startingPoint) {
        this.startingPoint = startingPoint;
    }

    @Override
    public String toString() {
        return "Track{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", pieces=" + pieces +
                ", lanes=" + lanes +
                ", startingPoint=" + startingPoint +
                '}';
    }
}

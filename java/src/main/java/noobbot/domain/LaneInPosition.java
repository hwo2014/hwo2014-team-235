package noobbot.domain;

/**
 * Created by vtajzich
 */
public class LaneInPosition {

    int startLaneIndex;
    int endLaneIndex;

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public void setStartLaneIndex(int startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }

    public void setEndLaneIndex(int endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }
}

package noobbot.domain;

/**
 * Created by vtajzich
 */
public class GameData {

    Race race;

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }
}

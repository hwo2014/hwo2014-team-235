package noobbot.bestlane;

import noobbot.domain.Lane;
import noobbot.domain.Piece;
import noobbot.domain.internal.LapPiece;

import java.util.*;

/**
 * User: mbednar
 * Date: 16.04.14
 * Time: 15:08
 */
public class StupidBrain {
    private final int lanesCount;
    private final Node[] startNodes;
    private Node finishNode;
    private List<Lane> lanes;
    private int laps;

    public StupidBrain(List<Lane> lanes, int laps) {
        this.lanes = lanes;
        this.laps = laps;
        this.lanesCount = lanes.size();
        this.startNodes = new Node[lanesCount];
    }

    public Map<LapPiece, Integer> getBestRace(List<LapPiece> piecesToEndOfRace, int actualLane) {
        List<Node> path = createShortestPath(piecesToEndOfRace, actualLane);

        Map<LapPiece, Integer> result = new LinkedHashMap<>();
        for (int i = 1; i < path.size() - 1; i++) {
            Node node = path.get(i);
            result.put(node.piece, node.laneIndex);
        }
        return result;
    }

    public int getBestLane(List<LapPiece> piecesToEndOfRace, int actualLane) {
        List<Node> path = createShortestPath(piecesToEndOfRace, actualLane);

        if (path.size() > 2) {
            Node node = path.get(2);
            return node.laneIndex;
        }
        return -1;
    }

    private List<Node> createShortestPath(List<LapPiece> piecesToEndOfRace, int actualLane) {
        createGraph(piecesToEndOfRace);
        computePaths(startNodes[actualLane]);
        return getShortestPathTo(finishNode);
    }

    private void createGraph(List<LapPiece> piecesToEndOfRace) {
        Node[] lastNodes = new Node[lanesCount];

        for (int i = 0; i < lastNodes.length; i++) {
            LapPiece startingPiece = new LapPiece(new Piece());
            startingPiece.setLap(1);
            startingPiece.setLength(0);
            startNodes[i] = new Node(startingPiece, i);
            lastNodes[i] = startNodes[i];
        }

        for (LapPiece lapPiece : piecesToEndOfRace) {
            //Initialize nodes for all lanes
            Node[] targetNodes = new Node[lastNodes.length];
            for (int i = 0; i < lastNodes.length; i++) {
                targetNodes[i] = new Node(lapPiece, i);
            }

            for (int i = 0; i < lastNodes.length; i++) {
                if (lapPiece.isSwitchPiece()) {
                    //connect each lane to each lane
                    for (int j = 0; j < lastNodes.length; j++) {
                        int distanceFromCenter = lanes.get(j).getDistanceFromCenter();
                        Edge edge = new Edge(targetNodes[j], lapPiece.getLengthForLane(distanceFromCenter));
                        lastNodes[i].addEdge(edge);
                    }
                } else {
                    int distanceFromCenter = lanes.get(i).getDistanceFromCenter();
                    Edge edge = new Edge(targetNodes[i], lapPiece.getLengthForLane(distanceFromCenter));
                    lastNodes[i].addEdge(edge);
                }
                lastNodes[i] = targetNodes[i];
            }
        }

        //Add finish Node
        LapPiece finishPiece = new LapPiece(new Piece());
        finishPiece.setLength(0);
        finishPiece.setLap(laps);
        finishNode = new Node(finishPiece, -1);
        for (int i = 0; i < lastNodes.length; i++) {
            Edge edge = new Edge(finishNode, 0);
            lastNodes[i].addEdge(edge);
        }


    }


    class Node implements Comparable<Node> {
        private final LapPiece piece;
        private int laneIndex;
        private List<Edge> edges = new ArrayList<>();
        private double minDistance = Double.POSITIVE_INFINITY;
        private Node previous;

        public Node(LapPiece piece, int laneIndex) {
            this.piece = piece;
            this.laneIndex = laneIndex;
        }

        public List<Edge> getEdges() {
            return edges;
        }

        public void addEdge(Edge edge) {
            edges.add(edge);
        }

        public int getLaneIndex() {
            return laneIndex;
        }

        @Override
        public String toString() {
            return "" + laneIndex + (edges.isEmpty() ? "" : edges.get(0).target.toString());
        }

        public int compareTo(Node other) {
            return Double.compare(minDistance, other.minDistance);
        }
    }

    class Edge {
        public final Node target;
        public final double weight;

        public Edge(Node argTarget, double argWeight) {
            target = argTarget;
            weight = argWeight;
        }
    }

    public void computePaths(Node source) {
        source.minDistance = 0.;
        PriorityQueue<Node> nodeQueue = new PriorityQueue<Node>();
        nodeQueue.add(source);

        while (!nodeQueue.isEmpty()) {
            Node u = nodeQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.getEdges()) {
                Node v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
                if (distanceThroughU < v.minDistance) {
                    nodeQueue.remove(v);
                    v.minDistance = distanceThroughU;
                    v.previous = u;
                    nodeQueue.add(v);
                }
            }
        }
    }

    public List<Node> getShortestPathTo(Node target) {
        List<Node> path = new ArrayList<Node>();
        for (Node node = target; node != null; node = node.previous)
            path.add(node);
        Collections.reverse(path);
        return path;
    }

}
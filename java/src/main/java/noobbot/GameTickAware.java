package noobbot;

import noobbot.domain.CarPosition;
import noobbot.message.CarPositionsMessage;

/**
 * Created by vtajzich
 */
public interface GameTickAware {

    default void onTick(CarPosition carPosition) {

    }

    default void onTick(CarPositionsMessage carPositionsMessage) {

    }
}

package noobbot;

import noobbot.domain.CarId;

import java.util.Map;

/**
 * User: mbednar
 * Date: 16.04.14
 * Time: 14:45
 */
public abstract class SensoredCar extends Car{
    private double speed;
    private double acceleration;
    private double deceleration;

    public SensoredCar(CarId id) {
        super(id);
    }
}

package noobbot;

import noobbot.message.AbstractMessage;

/**
 * Created by vtajzich
 */
public class Logger {

    public void fromServer(AbstractMessage message) {
        logToOutput(message, "from-server");
    }

    public void toServer(AbstractMessage message) {
        logToOutput(message, "to-server");
    }

    private void logToOutput(AbstractMessage message, String direction) {
        System.out.println("Direction: " + direction + ", message: " + message);
    }
}

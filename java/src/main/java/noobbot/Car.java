package noobbot;

import noobbot.domain.CarId;
import noobbot.domain.Dimension;

/**
 * Created by vtajzich
 */
public class Car {

    CarId id;
    Dimension dimension;

    public Car(CarId id) {
        this.id = id;
    }

    public CarId getId() {
        return id;
    }

    public void setId(CarId id) {
        this.id = id;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", dimension=" + dimension +
                '}';
    }
}

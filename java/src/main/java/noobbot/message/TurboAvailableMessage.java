package noobbot.message;

import noobbot.domain.Turbo;

/**
 * Created by vtajzich
 */
public class TurboAvailableMessage extends AbstractMessage<Turbo> {

    public TurboAvailableMessage() {
        this.msgType = MessageType.turboAvailable;
    }

    @Override
    Object getDataForJson() {
        return null;
    }
}

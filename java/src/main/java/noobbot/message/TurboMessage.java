package noobbot.message;

/**
 * Created by vtajzich
 */
public class TurboMessage extends AbstractMessage<String> {

    public TurboMessage(String message) {
        msgType = MessageType.turbo;
        data = message;
    }

    @Override
    Object getDataForJson() {
        return data;
    }
}

package noobbot.message;

public enum SwitchDirection {
    Left, Right, None;
}

package noobbot.message;

/**
 * Created by vtajzich
 */
public class JoinRaceMessage extends CreateRaceMessage {

    public JoinRaceMessage() {
        msgType = MessageType.joinRace;
    }

    public JoinRaceMessage(String name, String key, String trackName, String carCount, String password) {
        super(name, key, trackName, carCount, password);
        msgType = MessageType.joinRace;
    }
}

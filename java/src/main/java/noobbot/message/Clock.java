package noobbot.message;

import noobbot.GameTickAware;

/**
 * Created by vtajzich
 */
public class Clock implements GameTickAware {

    int time;

    public void onTick(CarPositionsMessage message) {
        time = message.gameTick;
    }

    public void onTick() {
        time++;
    }

    public int getTime() {
        return time;
    }

}

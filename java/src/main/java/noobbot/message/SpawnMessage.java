package noobbot.message;

/**
 * Created by vtajzich
 */
public class SpawnMessage extends AbstractMessage {

    public SpawnMessage() {
        msgType = MessageType.spawn;
    }

    @Override
    Object getDataForJson() {
        return "";
    }
}

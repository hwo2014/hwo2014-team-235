package noobbot.message;

/**
 * Created by vtajzich
 */
public class MsgWrapper {

    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }
}

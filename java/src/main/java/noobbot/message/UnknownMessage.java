package noobbot.message;

import java.util.Map;

/**
 * Created by vtajzich
 */
public class UnknownMessage extends AbstractMessage<Map> {

    public UnknownMessage() {
        msgType = MessageType.UNKNOWN;
    }

    @Override
    Object getDataForJson() {
        return "";
    }
}

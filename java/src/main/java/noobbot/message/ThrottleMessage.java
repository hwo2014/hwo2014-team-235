package noobbot.message;

/**
 * Created by vtajzich
 */
public class ThrottleMessage extends AbstractMessage {

    double value;

    public ThrottleMessage(double value) {
        this.msgType = MessageType.throttle;
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    @Override
    Object getDataForJson() {
        return value;
    }
}

package noobbot.message;

import java.util.Map;

/**
 * Created by vtajzich
 */
public class YourCarMessage extends AbstractMessage<Map> {

    public YourCarMessage() {
        msgType = MessageType.yourCar;
    }

    @Override
    Object getDataForJson() {
        return data;
    }

    public String getName() {
        return (String) data.get("name");
    }

    public String getColor() {
        return (String) data.get("color");
    }
}

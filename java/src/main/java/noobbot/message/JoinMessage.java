package noobbot.message;

import noobbot.domain.CarId;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vtajzich
 */
public class JoinMessage extends AbstractMessage<CarId> {

    String name;
    String key;

    public JoinMessage() {
        msgType = MessageType.join;
    }

    public JoinMessage(String name, String key) {
        this();
        this.name = name;
        this.key = key;
    }

    @Override
    Object getDataForJson() {

        Map map = new HashMap<>();
        map.put("name", name);
        map.put("key", key);

        return map;
    }
}

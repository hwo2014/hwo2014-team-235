package noobbot.message;

/**
 * Created by vtajzich
 */
public enum MessageType {

    join, ping, yourCar, gameInit, carPositions, throttle, gameStart, lapFinished, gameEnd, switchLane, UNKNOWN, crash, spawn, dnf, finish, createRace, joinRace, turboAvailable, turbo;

    public static MessageType valueOfAlias(String value) {

        for (MessageType type : values()) {
            if (type.name().equalsIgnoreCase(value)) {
                return type;
            }
        }

        return UNKNOWN;
    }
}

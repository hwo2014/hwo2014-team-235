package noobbot.message;

import noobbot.domain.CarPosition;

import java.util.List;

/**
 * Created by vtajzich
 */
public class CarPositionsMessage extends AbstractMessage<List<CarPosition>> {

    public CarPositionsMessage() {
        msgType = MessageType.carPositions;
    }

    public CarPositionsMessage(int tick) {
        this();
        setGameTick(tick);
    }

    @Override
    Object getDataForJson() {
        return data;
    }
}

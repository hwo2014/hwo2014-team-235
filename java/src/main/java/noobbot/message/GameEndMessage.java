package noobbot.message;

/**
 * Created by vtajzich
 */
public class GameEndMessage extends AbstractMessage {

    public GameEndMessage() {
        msgType = MessageType.gameEnd;
    }

    @Override
    Object getDataForJson() {
        return "";
    }
}

package noobbot.message;

import noobbot.domain.LapSummary;

/**
 * Created by vtajzich
 */
public class LapFinishedMessage extends AbstractMessage<LapSummary> {

    public LapFinishedMessage() {
        msgType = MessageType.lapFinished;
    }

    @Override
    Object getDataForJson() {
        return "";
    }
}

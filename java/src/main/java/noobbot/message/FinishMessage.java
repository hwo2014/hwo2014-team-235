package noobbot.message;

/**
 * Created by vtajzich
 */
public class FinishMessage extends AbstractMessage {

    public FinishMessage() {
        msgType = MessageType.finish;
    }

    @Override
    Object getDataForJson() {
        return "";
    }
}

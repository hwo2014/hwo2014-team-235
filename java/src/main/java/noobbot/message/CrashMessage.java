package noobbot.message;

/**
 * Created by vtajzich
 */
public class CrashMessage extends AbstractMessage {

    public CrashMessage() {
        msgType = MessageType.crash;
    }

    @Override
    Object getDataForJson() {
        return "";
    }
}

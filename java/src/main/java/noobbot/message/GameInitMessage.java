package noobbot.message;

import noobbot.domain.GameData;

/**
 * Created by vtajzich
 */
public class GameInitMessage extends AbstractMessage<GameData> {

    public GameInitMessage() {
        msgType = MessageType.gameInit;
    }

    @Override
    Object getDataForJson() {
        return null;
    }
}

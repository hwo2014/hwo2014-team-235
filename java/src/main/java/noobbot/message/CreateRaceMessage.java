package noobbot.message;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vtajzich
 */
public class CreateRaceMessage extends JoinMessage {

    private String trackName;
    private String carCount;
    private String password;

    public CreateRaceMessage() {
        msgType = MessageType.createRace;
    }

    public CreateRaceMessage(String name, String key, String trackName, String carCount, String password) {
        super(name, key);
        this.trackName = trackName;
        this.carCount = carCount;
        this.password = password;
        msgType = MessageType.createRace;
    }

    @Override
    Object getDataForJson() {

        Map map = new HashMap<>();
        map.put("name", name);
        map.put("key", key);

        Map message = new HashMap<>();
        message.put("botId", map);
        message.put("trackName", trackName);
        message.put("password", password);
        message.put("carCount", carCount);

        return message;
    }
}

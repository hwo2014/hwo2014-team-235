package noobbot.message;


public class SwitchLane extends AbstractMessage {
    private SwitchDirection lane;

    public SwitchLane(SwitchDirection lane) {
        this.msgType = MessageType.switchLane;
        this.lane = lane;
    }

    @Override
    Object getDataForJson() {
        return lane.name();
    }
}

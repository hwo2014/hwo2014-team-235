package noobbot.message;

/**
 * Created by vtajzich
 */
public class PingMessage extends AbstractMessage {

    public PingMessage() {
        msgType = MessageType.ping;
    }

    @Override
    Object getDataForJson() {
        return "";
    }
}

package noobbot.message;

import com.google.gson.Gson;

/**
 * Created by vtajzich
 */
public abstract class AbstractMessage<T> {

    MessageType msgType = MessageType.UNKNOWN;
    T data;
    int gameTick;

    public int getGameTick() {
        return gameTick;
    }

    public void setGameTick(int gameTick) {
        this.gameTick = gameTick;
    }

    public MessageType getMsgType() {
        return msgType;
    }

    public void setMsgType(MessageType msgType) {
        this.msgType = msgType;
    }

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public String toJson() {
        try {
            return new Gson().toJson(new MsgWrapper(getMessageTypeName(), getDataForJson()));

        } catch (Exception e) {
            System.out.println("Cannot convert object to json! " + getDataForJson());
        }

        return "";
    }

    private String getMessageTypeName() {

        if (msgType == null) {
            return MessageType.UNKNOWN.name();
        } else {
            return msgType.name();
        }
    }

    abstract Object getDataForJson();

    @Override
    public String toString() {
        try {
            return "msgType: " + getMessageTypeName() + ", data: " + getDataForJson() + ", gameTick: " + gameTick;
        } catch (Exception e) {
            return e.getMessage() + ": this = " + this.getClass();
        }
    }
}

package noobbot.message;

import java.util.Map;

/**
 * Created by vtajzich
 */
public class GameStartMessage extends AbstractMessage<Map> {

    public GameStartMessage() {
        msgType = MessageType.gameStart;
    }

    @Override
    Object getDataForJson() {
        return null;
    }
}

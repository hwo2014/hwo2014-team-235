package noobbot.fuzzy;

import noobbot.domain.Piece;
import noobbot.domain.PiecePosition;

/**
 * Created by vtajzich
 */
public interface PositionInPiece {

    FuzPosition getPosition(Piece piece, PiecePosition piecePosition);
}

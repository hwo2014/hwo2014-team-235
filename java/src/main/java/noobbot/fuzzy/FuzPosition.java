package noobbot.fuzzy;

/**
 * Created by vtajzich
 */
public enum FuzPosition {

    IN_THE_BEGINNING, NEAR_BEGINNIG, FIRST_QUATER, THIRD, HALF, TWO_THIRD, ALMOST, IN_THE_END
}

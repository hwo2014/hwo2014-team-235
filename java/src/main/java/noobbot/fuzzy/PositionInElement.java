package noobbot.fuzzy;

import noobbot.domain.PiecePosition;
import noobbot.domain.internal.TrackElement;

/**
 * Created by vtajzich
 */
public interface PositionInElement {

    FuzPosition getPosition(TrackElement trackElement, PiecePosition piecePosition);
}

package noobbot;

import noobbot.domain.CarId;

/**
 * Created by vtajzich
 */
public abstract class RacingCar extends Car {

    Logger logger;

    public RacingCar(String color, String name, Logger logger) {
        super(new CarId(name, color));
        this.logger = logger;
    }

//    abstract void accelerate(double value);
//
//    abstract void brake();

}

package noobbot;

import java.util.List;

/**
 * Created by vtajzich
 */
public class IndexUtil {

    public static int getPrevIndex(List list, int index) {

        int prevIndex = index;

        if (prevIndex == 0) {
            prevIndex = list.size() - 1;
        } else if (prevIndex > list.size() - 1) {
            prevIndex = list.size() - 1;
        }

        return prevIndex;
    }

    public static int getNextIndex(List list, int index) {

        int nextIndex = index + 1;

        if (nextIndex > list.size() - 1) {
            nextIndex = 0;
        }

        return nextIndex;
    }
}

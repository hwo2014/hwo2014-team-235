package noobbot;

import noobbot.domain.AbstractJsonTest;
import noobbot.domain.internal.TrackElement;
import noobbot.message.CarPositionsMessage;
import noobbot.message.GameInitMessage;
import noobbot.message.ThrottleMessage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by vtajzich
 */
@Ignore
public class CarBotTest extends AbstractJsonTest {

    CarBot car;

    @Before
    public void setUp() throws Exception {

        car = new CarBot("red", "Vitek", new Logger());

        GameInitMessage gameInitMessage = fromFile("carBot/gameInit.json", GameInitMessage.class);
        car.onMessage(gameInitMessage);
    }

    @Test
    public void straight() throws Exception {

        ThrottleMessage response = (ThrottleMessage) car.updateOnCarPosition(getPosition("position_straight"));

        assertTrue(1 == response.getValue());
    }

    @Test
    public void beforeCurve() throws Exception {

        ThrottleMessage response = (ThrottleMessage) car.updateOnCarPosition(getPosition("position_1_piece_before_curve"));

        assertTrue(0.3 == response.getValue());
    }

    @Test
    public void inCurve() throws Exception {

        ThrottleMessage response = (ThrottleMessage) car.updateOnCarPosition(getPosition("position_in_curve"));

        assertTrue(0.65 == response.getValue());
    }

    @Test
    public void in225Curve() throws Exception {

        ThrottleMessage response = (ThrottleMessage) car.updateOnCarPosition(getPosition("position_22.5_curve"));

        assertTrue(1 == response.getValue());
    }

    @Test
    public void afterGameInitThereShouldBePathElements() {

        List<TrackElement> elements = car.raceTrack.getElements();

        assertTrue(17 == elements.size());

        assertTrue(400 == elements.get(0).getLength());
        assertTrue(0 == elements.get(0).getAngle());

        assertTrue(400 == elements.get(1).getRadius());
        assertTrue(180 == elements.get(1).getAngle());

        assertTrue(200 == elements.get(2).getRadius());
        assertTrue(22.5 == elements.get(2).getAngle());

        assertTrue(200 == elements.get(3).getLength());
        assertTrue(0 == elements.get(3).getAngle());

        assertTrue(200 == elements.get(4).getRadius());
        assertTrue(-22.5 == elements.get(4).getAngle());

        assertTrue(200 == elements.get(5).getLength());
        assertTrue(0 == elements.get(5).getAngle());

        assertTrue(400 == elements.get(6).getRadius());
        assertTrue(-180 == elements.get(6).getAngle());

        assertTrue(100 == elements.get(7).getLength());
        assertTrue(0 == elements.get(7).getAngle());

        assertTrue(400 == elements.get(8).getRadius());
        assertTrue(180 == elements.get(8).getAngle());
    }

    private CarPositionsMessage getPosition(String file) {
        return fromFile("carBot/" + file + ".json", CarPositionsMessage.class);
    }
}

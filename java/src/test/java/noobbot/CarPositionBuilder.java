package noobbot;

import noobbot.domain.CarPosition;
import noobbot.domain.PiecePosition;

/**
 * Created by vtajzich
 */
public class CarPositionBuilder {

    private int index;
    private double positionInPiece;

    public static CarPositionBuilder newCarPosition() {
        return new CarPositionBuilder();
    }

    CarPositionBuilder withIndex(int index) {
        this.index = index;
        return this;
    }

    CarPositionBuilder withPositionInPiece(double positionInPiece) {
        this.positionInPiece = positionInPiece;
        return this;
    }

    CarPosition build() {
        return new CarPosition(new PiecePosition(index, positionInPiece));
    }

    public static CarPosition newCarPosition(int index, double positionInPiece) {
        return newCarPosition().withIndex(index).withPositionInPiece(positionInPiece).build();
    }
}

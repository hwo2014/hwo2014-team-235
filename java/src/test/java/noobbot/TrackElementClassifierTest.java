package noobbot;

import noobbot.domain.internal.TrackElement;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


public class TrackElementClassifierTest {

    @Test
    public void testClassifyStraight() throws Exception {

        TrackElement current = new TrackElementBuilder().straight(100, 4).build();
        TrackElement next = new TrackElementBuilder().straight(100, 2).build();

        double speed = TrackElementClassifier.classify(current, next);

        assertEquals(8d, speed);
    }

    @Test
    public void testClassifyStraightCurve() throws Exception {

        TrackElement current = new TrackElementBuilder().straight(100, 4).build();
        TrackElement next = new TrackElementBuilder().withCurve(45, 100, 4).build();

        double speed = TrackElementClassifier.classify(current, next);

        assertEquals(4d, speed);
    }

    @Test
    public void testClassifyStraightCurve2() throws Exception {

        TrackElement current = new TrackElementBuilder().straight(100, 4).build();
        TrackElement next = new TrackElementBuilder().withCurve(45, 50, 4).build();

        double speed = TrackElementClassifier.classify(current, next);

        assertEquals(2d, speed);
    }

    @Test
    public void testClassifyStraightCurve3() throws Exception {

        TrackElement current = new TrackElementBuilder().straight(100, 4).build();
        TrackElement next = new TrackElementBuilder().withCurve(22.5, 100, 4).build();

        double speed = TrackElementClassifier.classify(current, next);

        assertEquals(8d, speed);
    }

    @Test
    public void testClassifyStraightCurve4() throws Exception {

        TrackElement current = new TrackElementBuilder().straight(100, 4).build();
        TrackElement next = new TrackElementBuilder().withCurve(22.5, 30, 4).build();

        double speed = TrackElementClassifier.classify(current, next);

        assertEquals(3d, speed);
    }
}
package noobbot;

import noobbot.domain.AbstractJsonTest;
import noobbot.domain.Lane;
import noobbot.domain.Piece;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by vtajzich
 */
public class RaceTrackUSATest extends AbstractJsonTest {


    private RaceTrack raceTrack;

    @Before
    public void setUp() throws Exception {

        List<Piece> pieces = loadPieces("usa");

        raceTrack = new RaceTrack(pieces, Arrays.asList(new Lane[]{new Lane()}), 1);
    }

    @Test
    public void testNumberOfTrackElements() throws Exception {

        raceTrack.onTick(CarPositionBuilder.newCarPosition(0, 0));

        assertEquals(200d, raceTrack.getLengthTillNextCurve());

    }

    @Test
    public void testTrackType() {

        assertEquals(RaceTrack.Type.OVAL, raceTrack.getType());
    }

    List<Piece> loadPieces(String trackName) {
        return fromFile("tracks/" + trackName + ".json", PiecesWrapper.class).pieces;
    }


}

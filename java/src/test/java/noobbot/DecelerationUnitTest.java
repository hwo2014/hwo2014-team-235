package noobbot;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by vtajzich
 */
public class DecelerationUnitTest {

    DecelerationUnit unit;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testTimeLeftRaceStart() throws Exception {

        unit = new DecelerationUnit(0.1, 0.5, 0, 300, 1);

        assertEquals(Integer.MAX_VALUE, unit.timeLeft());
    }

    @Test
    public void testTimeLeftRaceStart2() throws Exception {

        unit = new DecelerationUnit(50, 0.5, 5.0, 300, 1);

        assertEquals(4, unit.timeLeft());
    }

    @Test
    public void testTimeLeftRaceStart3() throws Exception {

        unit = new DecelerationUnit(10, 1, 5.0, 300, 1);

        assertEquals(2, unit.timeLeft());
    }

    @Test
    public void testTimeLeftRaceStart4() throws Exception {

        unit = new DecelerationUnit(50, 0.5, 5.0, 300, 100);

        assertEquals(0, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve() throws Exception {

        unit = new DecelerationUnit(10, 1.0, 5.0, 0, 1);

        assertEquals(0, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve2() throws Exception {

        unit = new DecelerationUnit(10, 1.0, 5.0, 150, 1);

        assertEquals(2, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve3() throws Exception {

        unit = new DecelerationUnit(10, 1.0, 5.0, 72.57489418819088, 1);

        assertEquals(2, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve4() throws Exception {

        unit = new DecelerationUnit(8.209098941648804, 0.3, 0.2650734070555423, 12.100672715158296, 1);

        assertEquals(1, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve5() throws Exception {

        unit = new DecelerationUnit(8, 0.3, 1, 20, 1);

        assertEquals(4, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve6() throws Exception {

        unit = new DecelerationUnit(10, 5, 1, 100, 30);

        assertEquals(3, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve7() throws Exception {

        unit = new DecelerationUnit(0.2, 0.3, 0.5, 700, 0.2);

        assertEquals(Integer.MAX_VALUE, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve8() throws Exception {

        unit = new DecelerationUnit(8.49893783543179, 4.570962650159473, 0.09759102560386168, 116.23539916182999, 9d);

        assertEquals(9, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve9() throws Exception {

        unit = new DecelerationUnit(8.729316522700163, 1.5, 0.29162026699504473, 190.391334186675, 9d);

        assertEquals(7, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve10() throws Exception {

        unit = new DecelerationUnit(8.473196332571774, 1.5, 0.10335737435885761, 17.84144487038415, 9d);

        assertEquals(0, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve11() throws Exception {

        unit = new DecelerationUnit(0.2, 1.5, 0.05, 399.8, 0.2d);

        assertEquals(Integer.MAX_VALUE, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve12() throws Exception {

        unit = new DecelerationUnit(8.532909932205754, 7.020375152743492, 0.127389435115564, 20.55485647812094, 8.532909932205754);

        assertEquals(0, unit.timeLeft());
    }

    @Test
    public void testBeforeCurve13() throws Exception {

        unit = new DecelerationUnit(6.850081059262526, 1.5, 0.1188848813610548, 92.48130883016381, 6.850081059262526);

        assertEquals(10, unit.timeLeft());
    }

    @Test
    public void testTimeLeft() throws Exception {

        unit = new DecelerationUnit(60, 10, 10, 20000, 1);

        assertEquals(3, unit.timeLeft());
    }

    @Test
    public void testTimeLeftZeroLength() throws Exception {

        unit = new DecelerationUnit(500, 50, 10, 0, 1);

        assertEquals(0, unit.timeLeft());
    }
}

package noobbot.bestlane;

import noobbot.RaceTrack;
import noobbot.domain.AbstractJsonTest;
import noobbot.domain.Race;
import noobbot.domain.internal.LapPiece;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class BestLaneTest extends AbstractJsonTest {

    @Test
    public void keimolaLane0() throws Exception {

        Race race = fromFile("bestlane/keimola.json", Race.class);

        assertTrue(1 == race.getCars().size());
        assertNotNull(race.getRaceSession());
        assertNotNull(race.getTrack());

        Map<LapPiece, Integer> bestRace = new StupidBrain(race.getTrack().getLanes(), race.getRaceSession().getLaps())
                .getBestRace(RaceTrack.createWholeTrack(race.getTrack().getPieces(), race.getRaceSession().getLaps()), 0);
        Collection<Integer> values = bestRace.values();
        assertEquals("Piecies on race", 120, values.size());
        System.out.println(values);
    }

    @Test
    public void race2piecies3laps() throws Exception {

        Race race = fromFile("bestlane/2piecies.json", Race.class);

        Map<LapPiece, Integer> bestRace = new StupidBrain(race.getTrack().getLanes(), race.getRaceSession().getLaps())
                .getBestRace(RaceTrack.createWholeTrack(race.getTrack().getPieces(), race.getRaceSession().getLaps()), 0);

        Collection<Integer> values = new ArrayList<>(bestRace.values());
        assertEquals("Piecies on race", 6, values.size());
        System.out.println(values);
        assertThat(values, is(Arrays.asList(0, 0, 0, 0, 0, 0)));
    }

    @Test
    public void race3pieciesRightCurve() throws Exception {

        Race race = fromFile("bestlane/3pieciesRight.json", Race.class);

        Map<LapPiece, Integer> bestRace = new StupidBrain(race.getTrack().getLanes(), race.getRaceSession().getLaps())
                .getBestRace(RaceTrack.createWholeTrack(race.getTrack().getPieces(), race.getRaceSession().getLaps()), 0);

        Collection<Integer> values = new ArrayList<>(bestRace.values());
        assertEquals("Piecies on race", 3, values.size());
        System.out.println(values);
        assertThat(values, is(Arrays.asList(0, 1, 1)));
    }

    @Test
    public void race3pieciesRightCurveBestLane() throws Exception {

        Race race = fromFile("bestlane/3pieciesRight.json", Race.class);

        List<LapPiece> wholeTrack = RaceTrack.createWholeTrack(race.getTrack().getPieces(), race.getRaceSession().getLaps());
        StupidBrain stupidBrain = new StupidBrain(race.getTrack().getLanes(), race.getRaceSession().getLaps());
        int bestLane;

        bestLane = stupidBrain.getBestLane(wholeTrack, 0);
        assertEquals("First is 1", 1, bestLane);

        bestLane = stupidBrain.getBestLane(wholeTrack.subList(1, wholeTrack.size()), 0);
        assertEquals("Second is 1", 1, bestLane);

        bestLane = stupidBrain.getBestLane(wholeTrack.subList(1, wholeTrack.size()), 0);
        assertEquals("Third is 1", 1, bestLane);
    }

    @Test
    public void keimolaBestLane() throws Exception {

        Race race = fromFile("bestlane/keimola.json", Race.class);

        List<LapPiece> wholeTrack = RaceTrack.createWholeTrack(race.getTrack().getPieces(), race.getRaceSession().getLaps());
        StupidBrain stupidBrain = new StupidBrain(race.getTrack().getLanes(), race.getRaceSession().getLaps());
        int bestLane;

        bestLane = stupidBrain.getBestLane(wholeTrack, 0);
        assertEquals(0, bestLane);

        wholeTrack = wholeTrack.subList(1, wholeTrack.size());
        bestLane = stupidBrain.getBestLane(wholeTrack, 0);
        assertEquals(0, bestLane);

        wholeTrack = wholeTrack.subList(1, wholeTrack.size());
        bestLane = stupidBrain.getBestLane(wholeTrack, 0);
        assertEquals(1, bestLane);

        wholeTrack = wholeTrack.subList(1, wholeTrack.size());
        bestLane = stupidBrain.getBestLane(wholeTrack, 1);
        assertEquals(1, bestLane);

        wholeTrack = wholeTrack.subList(1, wholeTrack.size());
        bestLane = stupidBrain.getBestLane(wholeTrack, 1);
        assertEquals(1, bestLane);

        wholeTrack = wholeTrack.subList(1, wholeTrack.size());
        bestLane = stupidBrain.getBestLane(wholeTrack, 1);
        assertEquals(1, bestLane);
    }

    @Test
    public void race3pieciesLeftCurve() throws Exception {

        Race race = fromFile("bestlane/3pieciesLeft.json", Race.class);

        Map<LapPiece, Integer> bestRace = new StupidBrain(race.getTrack().getLanes(), race.getRaceSession().getLaps())
                .getBestRace(RaceTrack.createWholeTrack(race.getTrack().getPieces(), race.getRaceSession().getLaps()), 0);

        Collection<Integer> values = new ArrayList<>(bestRace.values());
        assertEquals("Piecies on race", 3, values.size());
        System.out.println(values);
        assertThat(values, is(Arrays.asList(0, 0, 0)));
    }

    @Test
    public void eight() throws Exception {

        Race race = fromFile("bestlane/eight.json", Race.class);

        Map<LapPiece, Integer> bestRace = new StupidBrain(race.getTrack().getLanes(), race.getRaceSession().getLaps())
                .getBestRace(RaceTrack.createWholeTrack(race.getTrack().getPieces(), race.getRaceSession().getLaps()), 0);

        Collection<Integer> values = new ArrayList<>(bestRace.values());
        assertEquals("Piecies on race", 30, values.size());
        System.out.println(values);
        assertThat(values, is(Arrays.asList(0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0)));
    }
}
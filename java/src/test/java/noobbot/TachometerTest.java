package noobbot;

import noobbot.domain.CarPosition;
import noobbot.domain.PiecePosition;
import noobbot.message.Clock;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by vtajzich
 */
public class TachometerTest {

    Tachometer tachometer;
    Clock clock;

    @Before
    public void setUp() throws Exception {

        clock = new Clock();
        tachometer = new Tachometer(clock);
    }

    @Test
    public void testNoTickNoSpeed() throws Exception {
        assertEquals(0d, tachometer.getSpeed());
    }

    @Test
    public void testFirstTick() throws Exception {

        tachometer.start();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 0)));

        assertEquals(0d, tachometer.getSpeed());
    }

    @Test
    public void testTick1() throws Exception {

        tachometer.start();

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 20)));

        assertEquals(20d, tachometer.getAcceleration());
        assertEquals(20d, tachometer.getSpeed());
    }

    @Test
    public void testTick2() throws Exception {

        tachometer.start();

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 20)));

        assertEquals(20d, tachometer.getAcceleration());

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 40)));

        assertEquals(0d, tachometer.getAcceleration());
        assertEquals(20d, tachometer.getSpeed());
    }

    @Test
    public void testTick3() throws Exception {

        tachometer.start();

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 5)));

        assertEquals(5d, tachometer.getAcceleration());

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 10)));

        assertEquals(0d, tachometer.getAcceleration());

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 20)));

        assertEquals(5d, tachometer.getAcceleration());
        assertEquals(10d, tachometer.getSpeed());
    }


    @Test
    public void testDeceleration() throws Exception {

        tachometer.start();

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 200)));

        assertEquals(200d, tachometer.getAcceleration());

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 300)));

        assertEquals(-100d, tachometer.getAcceleration());
        assertEquals(100d, tachometer.getKnownDecelerationPower());

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 350)));

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 350)));

        assertEquals(-50d, tachometer.getAcceleration());
        assertEquals(50d, tachometer.getKnownDecelerationPower());
    }

    @Test
    public void testSpeedBetweenPieces() throws Exception {

        tachometer.start();

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 5)));

        assertEquals(5d, tachometer.getAcceleration());
        assertEquals(5d, tachometer.getSpeed());

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(0, 10)));

        assertEquals(0.05, tachometer.getKnownDecelerationPower());
        assertEquals(0d, tachometer.getAcceleration());
        assertEquals(5d, tachometer.getSpeed());

        clock.onTick();

        tachometer.onTick(new CarPosition(new PiecePosition(1, 5)));

        assertEquals(0d, tachometer.getAcceleration());
        assertEquals(5d, tachometer.getSpeed());
    }

    @Test
    public void testDistancePerTick() {

        tachometer.start();

        clock.onTick();

        tachometer.onTick(CarPositionBuilder.newCarPosition(0, 5d));

        assertEquals(5d, tachometer.getDistancePerTick());

        clock.onTick();

        tachometer.onTick(CarPositionBuilder.newCarPosition(0, 10d));

        assertEquals(5d, tachometer.getDistancePerTick());

        clock.onTick();

        tachometer.onTick(CarPositionBuilder.newCarPosition(0, 30d));

        assertEquals(20d, tachometer.getDistancePerTick());
    }
}

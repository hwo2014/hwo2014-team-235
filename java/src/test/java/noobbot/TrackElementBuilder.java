package noobbot;

import noobbot.domain.Piece;
import noobbot.domain.internal.TrackElement;

/**
 * Created by vtajzich
 */
public class TrackElementBuilder {


    double length;
    int piecesCount;
    double angle;
    int radius;

    public TrackElementBuilder withCurve(double angle, int radius, int piecesCount) {

        this.angle = angle;
        this.radius = radius;
        this.piecesCount = piecesCount;

        return this;
    }

    public TrackElementBuilder straight(double length, int piecesCount) {

        this.length = length;
        this.piecesCount = piecesCount;

        return this;
    }

    public TrackElement build() {

        TrackElement element = new TrackElement(createPiece(0));

        for (int i = 1; i < piecesCount; i++) {
            element.getPieces().add(createPiece(i));
        }

        return element;
    }

    private Piece createPiece(int index) {

        Piece piece = null;

        if (length > 0) {
            piece = new Piece(index, length);
        } else {
            piece = new Piece(index, angle, radius);
        }

        return piece;
    }
}

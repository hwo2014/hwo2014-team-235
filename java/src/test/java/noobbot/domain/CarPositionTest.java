package noobbot.domain;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by vtajzich
 */
public class CarPositionTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        CarPosition carPosition = fromFile("carPosition.json", CarPosition.class);

        CarId id = carPosition.getId();

        assertEquals("AgileWorksVitek", id.getName());
        assertEquals("red", id.getColor());

        assertTrue(5 == carPosition.getAngle());

        PiecePosition position = carPosition.getPiecePosition();

        assertTrue(3 == position.getPieceIndex());
        assertEquals(4.268201721886722d, position.getInPieceDistance());
        assertTrue(23 == position.getLap());
    }
}

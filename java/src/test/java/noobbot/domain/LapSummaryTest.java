package noobbot.domain;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by vtajzich
 */
public class LapSummaryTest extends AbstractJsonTest {

    @Test
    public void testGetCar() throws Exception {

        LapSummary lapSummary = fromFile("lapSummary.json", LapSummary.class);

        CarId id = lapSummary.getCar();

        assertEquals("AgileWorksVitek", id.getName());
        assertEquals("red", id.getColor());

        LapTime lapLapTime = lapSummary.getLapTime();

        assertTrue(0 == lapLapTime.getLap());
        assertTrue(749 == lapLapTime.getTicks());
        assertTrue(12484 == lapLapTime.getMillis());

        RaceTime raceLapTime = lapSummary.getRaceTime();

        assertTrue(1 == raceLapTime.getLaps());
        assertTrue(750 == raceLapTime.getTicks());
        assertTrue(12500 == raceLapTime.getMillis());

        Ranking ranking = lapSummary.getRanking();

        assertTrue(1 == ranking.getOverall());
        assertTrue(1 == ranking.getFastestLap());
    }
}

package noobbot.domain;

import org.junit.Test;

import static junit.framework.Assert.*;

/**
 * Created by vtajzich
 */
public class TrackTest extends AbstractJsonTest {

    @Test
    public void testGetId() throws Exception {

        Track track = fromFile("track.json", Track.class);

        assertEquals("keimola", track.getId());
        assertEquals("Keimola", track.getName());

        assertTrue(40 == track.getPieces().size());
        assertTrue(2 == track.getLanes().size());

        assertNotNull(track.getStartingPoint());
    }
}

package noobbot.domain;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by vtajzich
 */
public class PositionTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        Position position = fromFile("position.json", Position.class);

        assertTrue(-300 == position.getX());
        assertTrue(-44 == position.getY());
    }
}

package noobbot.domain;

import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by vtajzich
 */
public class RaceTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        Race race = fromFile("race.json", Race.class);

        assertTrue(1 == race.getCars().size());
        assertNotNull(race.getRaceSession());
        assertNotNull(race.getTrack());
    }
}

package noobbot.domain;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by vtajzich
 */
public class RaceSessionTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        RaceSession raceSession = fromFile("raceSession.json", RaceSession.class);

        assertTrue(3 == raceSession.getLaps());
        assertTrue(60000 == raceSession.getMaxLapTimeMs());
        assertTrue(raceSession.isQuickRace());
    }
}

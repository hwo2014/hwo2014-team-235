package noobbot.domain;

import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by vtajzich
 */
public class PieceTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        Piece piece = fromFile("piece.json", Piece.class);

        assertTrue(100 == piece.getLength());
    }

    @Test
    public void parseCurved() throws Exception {

        Piece piece = fromFile("curvePiece.json", Piece.class);

        assertTrue(0 == piece.getLength());
        assertTrue(200 == piece.getRadius());
        assertTrue(22.5d == piece.getAngle());
        assertTrue(piece.isSwitchPiece());
    }

    @Test
    public void arcLenghtForRightCurve2Lanes() throws Exception {
        Piece piece = new Piece();
        piece.setAngle(90);
        piece.setRadius(100);

        double l0 = piece.getLengthForLane(-10);
        double l1 = piece.getLengthForLane(+10);
        Assert.assertTrue(l0 > l1);
    }

    @Test
    public void arcLenghtForLeftCurve2Lanes() throws Exception {
        Piece piece = new Piece();
        piece.setAngle(-90);
        piece.setRadius(100);

        double l0 = piece.getLengthForLane(-10);
        double l1 = piece.getLengthForLane(+10);
        Assert.assertTrue(l0 < l1);
    }
}

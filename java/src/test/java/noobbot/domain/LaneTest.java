package noobbot.domain;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by vtajzich
 */
public class LaneTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        Lane lane = fromFile("lane.json", Lane.class);

        assertTrue(-10 == lane.distanceFromCenter);
        assertTrue(0 == lane.getIndex());
    }
}

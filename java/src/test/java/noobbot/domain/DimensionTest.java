package noobbot.domain;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Created by vtajzich
 */
public class DimensionTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        Dimension dimension = fromFile("dimension.json", Dimension.class);

        assertTrue(40 == dimension.getLength());
        assertTrue(20 == dimension.getWidth());
        assertTrue(10 == dimension.getGuideFlagPosition());
    }
}

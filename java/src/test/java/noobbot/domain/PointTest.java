package noobbot.domain;

import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by vtajzich
 */
public class PointTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        Point point = fromFile("point.json", Point.class);

        assertTrue(90 == point.getAngle());
        assertNotNull(point.getPosition());
    }
}

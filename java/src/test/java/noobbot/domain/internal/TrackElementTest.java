package noobbot.domain.internal;

import noobbot.domain.Piece;
import noobbot.domain.PiecePosition;
import noobbot.fuzzy.FuzPosition;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by vtajzich
 */
public class TrackElementTest {

    @Test
    public void lengthOfStraightElement() throws Exception {

        TrackElement element = new TrackElement(new Piece(100, 0, 0, false));

        assertEquals(100d, element.getLength());
    }

    @Test
    public void lengthOfStraightElementsWithOnePieceOnly() throws Exception {

        TrackElement element = new TrackElement(new Piece(0, 100d));

        assertEquals(100d, element.getLength());
    }

    @Test
    public void lengthOfStraightElements() throws Exception {

        TrackElement element = new TrackElement(new Piece(0, 100d));
        element.getPieces().add(new Piece(1, 100d));
        element.getPieces().add(new Piece(2, 100d));

        assertEquals(300d, element.getLength());
        assertEquals(200d, element.getLength(1, -1));
        assertEquals(100d, element.getLength(0, -1));
    }

    @Test
    public void lengthOfStraightElementsDistance() throws Exception {

        TrackElement element = new TrackElement(new Piece(0, 100d));
        element.getPieces().add(new Piece(1, 100d));
        element.getPieces().add(new Piece(2, 100d));

        assertEquals(300d, element.getLength());
        assertEquals(200d, element.getLength(1, -1));
        assertEquals(200d, element.getLength(new PiecePosition(1, 0)));
        assertEquals(50d, element.getLength(new PiecePosition(0, 50)));
        assertEquals(75d, element.getLength(new PiecePosition(0, 25)));
        assertEquals(20d, element.getLength(new PiecePosition(0, 80)));
    }

    @Test
    public void lengthOfCurvedElement() throws Exception {

        TrackElement element = new TrackElement(new Piece(0, 180, 100, false));

        //angle * (PI / 180) * radius)
        //180 * PI / 180 * 100 = 314,15926536
        assertEquals(314d, Math.floor(element.getLength()));
    }

    @Test
    public void lengthOfCurvedElementMinus225() throws Exception {

        TrackElement element = new TrackElement(new Piece(0, 100, -22.5, false));

        assertEquals(39d, Math.floor(element.getLength()));
    }

    @Test
    public void lengthOfCurvedElementMinus45() throws Exception {

        TrackElement element = new TrackElement(new Piece(0, 200, -45, false));

        assertEquals(52.05480014821107, element.getLength(0, 66.86088497900593, true));
    }

    @Test
    public void lengthOfStraightElement2() throws Exception {

        TrackElement element = new TrackElement(new Piece(100, 0, 0, false));

        assertEquals(34.742609314701554, element.getLength(0, 65.25739068529845, true));
    }

    @Test
    public void lengthOfStraightElement3() throws Exception {

        TrackElement element = new TrackElement(new Piece(100, 0, 0, false));

        assertEquals(8d, element.getLength(0, 92, true));
    }

    @Test
    public void lengthOfCurvedElement4elements() throws Exception {

        TrackElement element = new TrackElement(new Piece(0, 45, 100, false));
        element.getPieces().add(new Piece(0, 45, 100, false));
        element.getPieces().add(new Piece(0, 45, 100, false));
        element.getPieces().add(new Piece(0, 45, 100, false));

        //angle * (PI / 180) * radius)
        //180 * PI / 180 * 100 = 314,15926536
        assertEquals(314.1592653589793, element.getLength());
    }

    /*
    TrackElement{pieces=[Piece{index=12, length=100.0, switchPiece=false, radius=0, angle=0.0}, Piece{index=13, length=100.0, switchPiece=true, radius=0, angle=0.0}]},
    TrackElement{pieces=[Piece{index=14, length=0.0, switchPiece=false, radius=100, angle=-45.0},
    Piece{index=15, length=0.0, switchPiece=false, radius=100, angle=-45.0},
    Piece{index=16, length=0.0, switchPiece=false, radius=100, angle=-45.0},
    Piece{index=17, length=0.0, switchPiece=false, radius=100, angle=-45.0}]},
     */

    @Test
    public void lengthOfCurvedElement5() throws Exception {

        TrackElement element = new TrackElement(new Piece(12, 100d));
        element.getPieces().add(new Piece(13, 100d));

        TrackElement element2 = new TrackElement(new Piece(14, -45.0, 100));
        element2.getPieces().add(new Piece(15, -45.0, 100));
        element2.getPieces().add(new Piece(16, -45.0, 100));
        element2.getPieces().add(new Piece(17, -45.0, 100));

        assertEquals(100.9393249297778, element.getLength(12, 99.0606750702222, true));
        assertEquals(93.07363492266957, element.getLength(13, 6.926365077330431, true));
    }

    @Test
    public void lengthOfCurvedElement4elementsInHalf() throws Exception {

        TrackElement element = new TrackElement(new Piece(0, 45, 100));
        element.getPieces().add(new Piece(1, 45, 100));
        element.getPieces().add(new Piece(2, 45, 100));
        element.getPieces().add(new Piece(3, 45, 100));

        //angle * (PI / 180) * radius)
        //180 * PI / 180 * 100 = 314,15926536
        assertEquals(235.61944901923448, element.getLength(2, 0));
        assertEquals(314d, Math.floor(element.getLength()));
    }

    @Test
    public void findPieceForPosition() {

        TrackElement element = new TrackElement(new Piece(0, 200d));

        element.getPieces().add(new Piece(1, 100d));
        element.getPieces().add(new Piece(2, 100d));
        element.getPieces().add(new Piece(3, 100d));
        element.getPieces().add(new Piece(4, 100d));

        Piece piece = element.getPieceFor(new PiecePosition(2));

        assertEquals(2, piece.getIndex());
    }

    /*
    Direction: from-server, message: msgType: carPositions, data: [CarPosition{id={name='VitekAI', color='red'}, angle=-0.010086520251252876, piecePosition={pieceIndex=13, inPieceDistance=95.08297568179236, lap=0}}], gameTick: 238
Speed: 8.293106117221242, acceleration: 0.03483456903630611, bp: 0.5
params: 1.0, 0.5, 4.917024318207638
Time left before curve: 2
Final: ACCELERATING
Direction: to-server, message: msgType: throttle, data: 1.0, gameTick: 0
Direction: from-server, message: msgType: carPositions, data: [CarPosition{id={name='VitekAI', color='red'}, angle=-0.010835199648143678, piecePosition={pieceIndex=14, inPieceDistance=3.410219676669172, lap=0}}], gameTick: 239
ACCELERATING
params: 1.0, 0.5, 314.1592653589793
     */

    @Ignore
    @Test
    public void fuzPositions() {

        TrackElement element = new TrackElement(new Piece(100, 0));

        element.getPieces().add(new Piece(100, 1));
        element.getPieces().add(new Piece(100, 2));
        element.getPieces().add(new Piece(100, 3));


        FuzPosition fuzPosition = element.getPosition(new PiecePosition(0, 0));

        assertEquals(FuzPosition.IN_THE_BEGINNING, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(0, 46));

        assertEquals(FuzPosition.NEAR_BEGINNIG, fuzPosition);


        fuzPosition = element.getPosition(new PiecePosition(0, 99));

        assertEquals(FuzPosition.FIRST_QUATER, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(1, 0));

        assertEquals(FuzPosition.FIRST_QUATER, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(1, 10));

        assertEquals(FuzPosition.FIRST_QUATER, fuzPosition);


        fuzPosition = element.getPosition(new PiecePosition(1, 28));

        assertEquals(FuzPosition.THIRD, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(1, 33));

        assertEquals(FuzPosition.THIRD, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(1, 50));

        assertEquals(FuzPosition.THIRD, fuzPosition);


        fuzPosition = element.getPosition(new PiecePosition(1, 90));

        assertEquals(FuzPosition.HALF, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(2, 0));

        assertEquals(FuzPosition.HALF, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(2, 10));

        assertEquals(FuzPosition.HALF, fuzPosition);


        fuzPosition = element.getPosition(new PiecePosition(2, 50));

        assertEquals(FuzPosition.TWO_THIRD, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(2, 66));

        assertEquals(FuzPosition.TWO_THIRD, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(2, 80));

        assertEquals(FuzPosition.TWO_THIRD, fuzPosition);


        fuzPosition = element.getPosition(new PiecePosition(3, 60));

        assertEquals(FuzPosition.ALMOST, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(3, 70));

        assertEquals(FuzPosition.ALMOST, fuzPosition);

        fuzPosition = element.getPosition(new PiecePosition(3, 90));

        assertEquals(FuzPosition.ALMOST, fuzPosition);


        fuzPosition = element.getPosition(new PiecePosition(3, 100));

        assertEquals(FuzPosition.IN_THE_END, fuzPosition);

    }
}

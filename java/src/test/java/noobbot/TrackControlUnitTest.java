package noobbot;

import noobbot.domain.CarPosition;
import noobbot.domain.Lane;
import noobbot.domain.Piece;
import noobbot.domain.internal.TrackElement;
import noobbot.message.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by vtajzich
 */
public class TrackControlUnitTest {

    TrackControlUnit unit;
    Clock clock;
    Tachometer tachometer;
    AtomicInteger tick;

    @Before
    public void setUp() {

        tick = new AtomicInteger();

        clock = new Clock();
        tachometer = new Tachometer(clock);

        unit = new TrackControlUnit();
    }

    @Test
    public void testForceBreak() throws Exception {

        unit.angleIncrements.add(20.0);
        unit.angleIncrements.add(25.0);
        unit.angleIncrements.add(30.0);
        unit.angleIncrements.add(40.0);

        assertTrue(unit.forceBreak(Curve.Ostra, 10d));
    }

    @Test
    public void testForceBreak2() throws Exception {

        unit.angleIncrements.add(20.0);
        unit.angleIncrements.add(20.0000001);

        assertFalse(unit.forceBreak(Curve.Ostra, 0.0000001));
    }

    @Test
    public void testForceBreak3() throws Exception {

        unit.angleIncrements.add(0d);
        unit.angleIncrements.add(0.044016663994394016);

        assertFalse(unit.forceBreak(Curve.Ostra, 0.044016663994394016));
    }

    @Test
    public void testFilterMessages() {

        DnfMessage dnfMessage = new DnfMessage();
        FinishMessage finishMessage = new FinishMessage();
        ThrottleMessage t5 = new ThrottleMessage(5);
        ThrottleMessage t3 = new ThrottleMessage(3);
        ThrottleMessage t1 = new ThrottleMessage(1);
        ThrottleMessage t0_2 = new ThrottleMessage(0.2);

        List<AbstractMessage> input = new ArrayList<>();
        input.add(dnfMessage);
        input.add(finishMessage);
        input.add(t5);
        input.add(t3);
        input.add(t1);
        input.add(t0_2);

        List<AbstractMessage> expected = new ArrayList<>();
        expected.add(dnfMessage);
        expected.add(finishMessage);
        expected.add(t0_2);

        List<AbstractMessage> output = unit.leaveOnlyLowestThrottleMessage(input);

        assertEquals(expected, output);
    }

    @Test
    public void testClassifyTrackJustStraight() {

        List<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(0, 200));
        pieces.add(new Piece(1, 200));
        pieces.add(new Piece(2, 200));

        List<Lane> lanes = new ArrayList<>();
        lanes.add(new Lane());

        RaceTrack raceTrack = new RaceTrack(pieces, lanes, 3);

        unit = new TrackControlUnit(tachometer, raceTrack, clock);

        unit.classifyTrack();

        assertSpeeds(60);
    }

    @Test
    public void testClassifyTrackOval() {

        List<Piece> pieces = new ArrayList<Piece>();
        pieces.add(new Piece(0, 200));
        pieces.add(new Piece(1, 45, 100));
        pieces.add(new Piece(2, 200));
        pieces.add(new Piece(3, 45, 100));

        List<Lane> lanes = new ArrayList<>();
        lanes.add(new Lane());

        RaceTrack raceTrack = new RaceTrack(pieces, lanes, 3);

        unit = new TrackControlUnit(tachometer, raceTrack, clock);

        unit.classifyTrack();

        assertSpeed(0, 8);
        assertSpeed(1, 4);
        assertSpeed(2, 8);
        assertSpeed(3, 4);
    }

    @Test
    public void testClassifyTrackCombo1() {

        List<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(0, 200));
        pieces.add(new Piece(1, 45, 100));
        pieces.add(new Piece(2, 50));
        pieces.add(new Piece(3, 45, 100));
        pieces.add(new Piece(4, 200));
        pieces.add(new Piece(5, 45, 100));

        List<Lane> lanes = new ArrayList<>();
        lanes.add(new Lane());

        RaceTrack raceTrack = new RaceTrack(pieces, lanes, 3);

        unit = new TrackControlUnit(tachometer, raceTrack, clock);

        unit.classifyTrack();

        assertSpeed(0, 8);
        assertSpeed(1, 4);
        assertSpeed(2, 4);
        assertSpeed(3, 4);
        assertSpeed(4, 8);
        assertSpeed(5, 4);
    }

    @Test
    public void testClassifyTrackCombo2() {

        List<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(0, 200));
        pieces.add(new Piece(1, 45, 100));
        pieces.add(new Piece(2, 100));
        pieces.add(new Piece(3, 22.5, 100));
        pieces.add(new Piece(4, 45, 50));
        pieces.add(new Piece(5, 45, 50));
        pieces.add(new Piece(6, 45, 50));
        pieces.add(new Piece(7, 45, 50));
        pieces.add(new Piece(4, 200));
        pieces.add(new Piece(5, 45, 100));

        List<Lane> lanes = new ArrayList<>();
        lanes.add(new Lane());

        RaceTrack raceTrack = new RaceTrack(pieces, lanes, 3);

        unit = new TrackControlUnit(tachometer, raceTrack, clock);

        unit.classifyTrack();

        assertSpeed(0, 5);
        assertSpeed(1, 2);
        assertSpeed(2, 2);
        assertSpeed(3, 2);
        assertSpeed(4, 2);
        assertSpeed(5, 5);
        assertSpeed(6, 2);
    }

    @Test
    public void testReClassifyTrackCombo() {

        List<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(0, 200));
        pieces.add(new Piece(1, 45, 100));
        pieces.add(new Piece(2, 100));
        pieces.add(new Piece(3, 22.5, 100));
        pieces.add(new Piece(4, 45, 50));
        pieces.add(new Piece(5, 45, 50));
        pieces.add(new Piece(6, 45, 50));
        pieces.add(new Piece(7, 45, 50));
        pieces.add(new Piece(4, 200));
        pieces.add(new Piece(5, 45, 100));

        List<Lane> lanes = new ArrayList<>();
        lanes.add(new Lane());

        RaceTrack raceTrack = new RaceTrack(pieces, lanes, 3);

        unit = new TrackControlUnit(tachometer, raceTrack, clock);

        unit.classifyTrack();

        unit.throttleForTrackElements.values().stream().forEach(t -> {
            t.setSpeed(4d);
            t.addAngle(0.9);
        });

        unit.reClassifyTrack();

        assertSpeed(0, 12);
        assertSpeed(1, 4);
        assertSpeed(2, 12);
        assertSpeed(3, 4);
        assertSpeed(4, 4);
        assertSpeed(5, 12);
        assertSpeed(6, 4);
    }

    @Ignore
    @Test
    public void testReClassifyTrack() {

        List<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(0, 200));
        pieces.add(new Piece(1, 200));
        pieces.add(new Piece(2, 200));

        List<Lane> lanes = new ArrayList<>();
        lanes.add(new Lane());

        RaceTrack raceTrack = new RaceTrack(pieces, lanes, 3);

        unit = new TrackControlUnit(tachometer, raceTrack, clock);

        assertSpeeds(1.5d);

        //lap 1
        doTick(0, 0);
        doTick(0, 100);
        doTick(1, 0);
        doTick(1, 100);
        doTick(2, 0);
        doTick(2, 100);

        unit.reClassifyTrack();

        assertSpeeds(1.5d);

        //lap 2
        doTick(0, 0);
        doTick(0, 100);
        doTick(1, 0);
        doTick(1, 100);
        doTick(2, 0);
        doTick(2, 100);

        unit.reClassifyTrack();

        assertSpeeds(1.5d);

        //lap 3
        doTick(0, 0);
        doTick(0, 100);
        doTick(1, 0);
        doTick(1, 100);
        doTick(2, 0);
        doTick(2, 100);

        unit.reClassifyTrack();

        assertSpeeds(1.5d);
    }

    void doTick(int index, double position) {

        CarPosition carPosition = CarPositionBuilder.newCarPosition(index, position);

        clock.onTick(new CarPositionsMessage(tick.getAndIncrement()));
        tachometer.onTick(carPosition);
        unit.onTick(carPosition);

        unit.control();
    }

    void assertSpeeds(double expectedSpeed) {

        unit.throttleForTrackElements.values().stream().mapToDouble(t -> t.getSpeed()).forEach(d -> assertEquals(expectedSpeed, d));
    }

    void assertSpeed(int index, double expectedSpeed) {

        TrackElement te = unit.raceTrack.getElements().get(index);

        CarBot.ThrottleSpeed throttleSpeed = unit.throttleForTrackElements.get(new ThrottleTeKey(te, te.getFirstPiece()));

        assertEquals(expectedSpeed, throttleSpeed.getSpeed());
    }
}

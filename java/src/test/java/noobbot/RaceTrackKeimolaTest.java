package noobbot;

import junit.framework.TestCase;
import noobbot.domain.AbstractJsonTest;
import noobbot.domain.Lane;
import noobbot.domain.Piece;
import noobbot.domain.internal.TrackElement;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

/**
 * Created by vtajzich
 */
public class RaceTrackKeimolaTest extends AbstractJsonTest {

    RaceTrack raceTrack;

    @Before
    public void setUp() throws Exception {

        List<Piece> pieces = loadPieces("keimola");

        raceTrack = new RaceTrack(pieces, Arrays.asList(new Lane[]{new Lane()}), 1);
    }

    @Test
    public void testMovedToNextElement() throws Exception {

        assertFalse(raceTrack.movedToNextElement());

        raceTrack.onTick(CarPositionBuilder.newCarPosition(4, 0d));

        assertTrue(raceTrack.movedToNextElement());
    }

    @Test
    public void testIsCurve() throws Exception {

        assertFalse(raceTrack.isCurve());
    }

    @Test
    public void testIsCurve2() throws Exception {

        raceTrack.onTick(CarPositionBuilder.newCarPosition(4, 0d));

        assertTrue(raceTrack.isCurve());
    }

    @Test
    public void testGetNextThrottleTeKey() throws Exception {

        TrackElement nextElement = raceTrack.getNextElement();

        ThrottleTeKey key = new ThrottleTeKey(nextElement, nextElement.getFirstPiece());

        assertEquals(key, raceTrack.getNextThrottleTeKey());


        raceTrack.onTick(CarPositionBuilder.newCarPosition(0, 0d));

        assertEquals(key, raceTrack.getNextThrottleTeKey());


        raceTrack.onTick(CarPositionBuilder.newCarPosition(4, 0d));

        nextElement = raceTrack.getNextElement();

        key = new ThrottleTeKey(nextElement, nextElement.getFirstPiece());

        assertEquals(key, raceTrack.getNextThrottleTeKey());
    }

    @Test
    public void testGetNextElement() {

        TrackElement nextElement = raceTrack.elements.get(1);

        assertEquals(nextElement, raceTrack.getNextElement());


        raceTrack.onTick(CarPositionBuilder.newCarPosition(0, 0d));

        assertEquals(nextElement, raceTrack.getNextElement());


        nextElement = raceTrack.elements.get(2);

        raceTrack.onTick(CarPositionBuilder.newCarPosition(4, 0d));

        assertEquals(nextElement, raceTrack.getNextElement());
    }

    @Test
    public void testGetNextElementInTheEnd() {

        TrackElement nextElement = raceTrack.elements.get(0);

        raceTrack.onTick(CarPositionBuilder.newCarPosition(39, 0d));

        assertEquals(nextElement, raceTrack.getNextElement());
    }

    @Test
    public void testGetLengthTillNextCurveInTheBeginning() throws Exception {

        double length = raceTrack.getLengthTillNextCurve();

        assertEquals(400d, length);

        raceTrack.onTick(CarPositionBuilder.newCarPosition(0, 0d));

        length = raceTrack.getLengthTillNextCurve();

        assertEquals(400d, length);
    }

    @Test
    public void testGetLengthTillNextCurveStraightJustBeforeBigCurve() throws Exception {

        raceTrack.onTick(CarPositionBuilder.newCarPosition(2, 50d));

        double length = raceTrack.getLengthTillNextCurve();

        assertEquals(150d, length);
    }

    @Test
    public void testGetLengthTillNextCurveJustAfterBigCurveCoupleSmallCurvesAhead() throws Exception {

        raceTrack.onTick(CarPositionBuilder.newCarPosition(7, 100d));

        TrackElement currentElement = raceTrack.getCurrentElement();

        assertEquals(0d, currentElement.getLength(7, 100d, true));

        double length = raceTrack.getLengthTillNextCurve();

        //400 + 2 * (200 radius, 22.5 angle)
        //400 + 2 * 78.53981633974483
        assertEquals(400 + 2 * 78.53981633974483, length);
    }

    @Test
    public void testRealCase1() {

        //Direction: from-server, message: msgType: carPositions, data:
        // [CarPosition{id={name='VitekAI', color='red'}, angle=-1.1460404550928203,
        // piecePosition={pieceIndex=31, inPieceDistance=2.8085862875665413, lap=0}}], gameTick: 508
        raceTrack.onTick(CarPositionBuilder.newCarPosition(31, 2.8085862875665413));

        double length = raceTrack.getLengthTillNextCurve();

        assertEquals(311.9534068469813, length);
    }

    @Test
    public void testGetThrottleTeKey() throws Exception {

        ThrottleTeKey key = new ThrottleTeKey(raceTrack.getCurrentElement(), new Piece(0));

        assertEquals(key, raceTrack.getThrottleTeKey());

        raceTrack.onTick(CarPositionBuilder.newCarPosition(0, 0d));

        assertEquals(key, raceTrack.getThrottleTeKey());

        raceTrack.onTick(CarPositionBuilder.newCarPosition(4, 0d));

        key = new ThrottleTeKey(raceTrack.getCurrentElement(), new Piece(4));

        assertEquals(key, raceTrack.getThrottleTeKey());
    }

    @Test
    public void testTrackType() {

        TestCase.assertEquals(RaceTrack.Type.NOT_EXTRA_CURVES, raceTrack.getType());
    }

    List<Piece> loadPieces(String trackName) {
        return fromFile("tracks/" + trackName + ".json", PiecesWrapper.class).pieces;
    }
}

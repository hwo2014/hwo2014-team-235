package noobbot;

import noobbot.domain.AbstractJsonTest;
import noobbot.domain.Lane;
import noobbot.domain.Piece;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by vtajzich
 */
public class RaceTrackGermanyTest extends AbstractJsonTest {


    private RaceTrack raceTrack;

    @Before
    public void setUp() throws Exception {

        List<Piece> pieces = loadPieces("germany");

        raceTrack = new RaceTrack(pieces, Arrays.asList(new Lane[]{new Lane()}), 1);
    }

    @Test
    public void testNumberOfTrackElements() throws Exception {

        raceTrack.onTick(CarPositionBuilder.newCarPosition(45, 7.518691169836193));

        assertEquals(92.48130883016381, raceTrack.getLengthTillNextCurve());

    }

    List<Piece> loadPieces(String trackName) {
        return fromFile("tracks/" + trackName + ".json", PiecesWrapper.class).pieces;
    }
}

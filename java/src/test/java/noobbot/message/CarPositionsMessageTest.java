package noobbot.message;

import noobbot.domain.AbstractJsonTest;
import noobbot.domain.CarPosition;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by vtajzich
 */
public class CarPositionsMessageTest extends AbstractJsonTest {

    @Test
    public void parse() throws Exception {

        CarPositionsMessage message = fromFile("carPositionsMessage.json", CarPositionsMessage.class);

        assertNotNull(message);

        List<CarPosition> carPositions = message.getData();

        assertTrue(2 == carPositions.size());

        CarPosition carPosition = carPositions.get(0);

        assertNotNull(carPosition);
        assertNotNull(carPosition.getPiecePosition().getLane());
        assertEquals(0, carPosition.getPiecePosition().getLane().getStartLaneIndex());
        assertEquals(0, carPosition.getPiecePosition().getLane().getEndLaneIndex());

        CarPosition carPosition2 = carPositions.get(1);

        assertNotNull(carPosition2);
        assertNotNull(carPosition2.getPiecePosition().getLane());
        assertEquals(1, carPosition2.getPiecePosition().getLane().getStartLaneIndex());
        assertEquals(1, carPosition2.getPiecePosition().getLane().getEndLaneIndex());
    }
}

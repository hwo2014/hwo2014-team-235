package noobbot.message;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by vtajzich
 */
public class JoinMessageTest {

    JoinMessage joinMessage;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testToJson() throws Exception {

        joinMessage = new JoinMessage("AgileWorksVitek", "ddqHUW+iP9/9Uw");

        String json = joinMessage.toJson();

        /*
        "msgType": "join",
        "data": {
            "name": "AgileWorksVitek",
            "key": "ddqHUW+iP9/9Uw"
        }
         */
        assertEquals("{\"msgType\":\"join\",\"data\":{\"name\":\"AgileWorksVitek\",\"key\":\"ddqHUW+iP9/9Uw\"}}", json);
    }
}

package noobbot;

import junit.framework.TestCase;
import noobbot.domain.AbstractJsonTest;
import noobbot.domain.Lane;
import noobbot.domain.Piece;
import noobbot.domain.internal.TrackElement;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by vtajzich
 */
public class RaceTrackFranceTest extends AbstractJsonTest {

    private RaceTrack raceTrack;
    TrackControlUnit unit;

    @Before
    public void setUp() throws Exception {

        List<Piece> pieces = loadPieces("france");

        raceTrack = new RaceTrack(pieces, Arrays.asList(new Lane[]{new Lane()}), 1);

        unit = new TrackControlUnit(null, raceTrack, null);
    }

    @Test
    public void testNumberOfTrackElements() throws Exception {

        raceTrack.onTick(CarPositionBuilder.newCarPosition(0, 0));

        assertEquals(196d, raceTrack.getLengthTillNextCurve());

    }

    @Test
    public void testRaceClassification() {

        // Track elements
        // 0 - 0, 196
        // 1 - 22.5 (200)
        // 2 - 45 (100)
        // 3 - 0, 53
        // 4 - -22.5 (200)
        // 5 - -45 (50)
        // 6 - 45 (100)
        // 7 - -45 (50)
        // 8 - -22.5 (200)
        // 9 - 0, 188
        // 10 - 45 (50)
        // 11 - 0, 198
        // 12 - -45 (100)
        // 13 - 45 (100)
        // 14 - 22.5 (200)
        // 15 - 0, 53
        // 16 - 22.5 (200)
        // 17 - 45 (100)
        // 18 - 22.5 (200)
        // 19 - 0, 561


        assertSpeed(0, 5);
        assertSpeed(1, 4);
        assertSpeed(2, 2);
        assertSpeed(3, 2);
        assertSpeed(4, 2);
        assertSpeed(5, 2); //1
        assertSpeed(6, 2);
        assertSpeed(7, 2);
        assertSpeed(8, 4);
        assertSpeed(9, 4);
        assertSpeed(10, 2);
        assertSpeed(11, 4);
        assertSpeed(12, 2);
        assertSpeed(13, 2);
        assertSpeed(14, 4);
        assertSpeed(15, 5);
        assertSpeed(16, 4);
        assertSpeed(17, 2);
        assertSpeed(18, 4);
        assertSpeed(19, 8);

        unit.reClassifyTrack();

        assertSpeed(0, 8);
        assertSpeed(1, 5.2);
        assertSpeed(2, 2);
        assertSpeed(3, 8);
        assertSpeed(4, 4);
        assertSpeed(5, 2); //1
        assertSpeed(6, 2);
        assertSpeed(7, 2);
        assertSpeed(8, 6);
        assertSpeed(9, 4);
        assertSpeed(10, 2);
        assertSpeed(11, 8);
        assertSpeed(12, 2);
        assertSpeed(13, 2);
        assertSpeed(14, 5.2);
        assertSpeed(15, 8);
        assertSpeed(16, 5.2);
        assertSpeed(17, 2);
        assertSpeed(18, 5.2);
        assertSpeed(19, 12);

        unit.onTick(CarPositionBuilder.newCarPosition(19, 93d));
    }

    @Test
    public void testTrackType() {

        TestCase.assertEquals(RaceTrack.Type.GENERIC, raceTrack.getType());
    }

    void assertSpeed(int index, double expectedSpeed) {

        TrackElement te = unit.raceTrack.getElements().get(index);

        CarBot.ThrottleSpeed throttleSpeed = unit.throttleForTrackElements.get(new ThrottleTeKey(te, te.getFirstPiece()));

        assertEquals(expectedSpeed, throttleSpeed.getSpeed());
    }

    List<Piece> loadPieces(String trackName) {
        return fromFile("tracks/" + trackName + ".json", PiecesWrapper.class).pieces;
    }
}
